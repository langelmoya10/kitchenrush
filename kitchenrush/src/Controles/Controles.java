/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controles;

import Modelo.Jugador;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Clase la cual setea la configuracion desde un .txt para que el jugador se pueda mover dentro del juego.
 * @author CHRISTIN
 */
public class Controles {
    private String arriba;
    private String abajo;
    private String izquierda;
    private String derecha;
    private String[] control;

    /**
     *Constructor por defecto.
     */
    public Controles() {
    }

    /**
     * Metodo que me permite leer un archivo .txt y almacenarlo en un arreglo.
     * @param fuente Direccion relativa del archivo controles.txt
     * @return Un arreglo en la cual cada indice posee informacion de las teclas que dan movimiento al jugador.
     */
    public ArrayList<String> leerControles(String fuente) {
        ArrayList<String> listaControles = new ArrayList<>();
        BufferedReader inputStream = null;
        String line = null;
        try {
            inputStream = new BufferedReader(new FileReader(fuente));
            while ((line = inputStream.readLine()) != null) {
                listaControles.add(line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        } catch (IOException e) {
            System.out.println("Error al momento de leer el archivo.");
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
            }
        }
        return listaControles;
    }
    
    /**
     *Metodo sobrecargado que define las teclas para el movimiento de los jugadores.
     * @param fuente Direccion relativa del archivo controles.txt
     * @param jugador identificador del jugador (1 o 2) ya que cada jugador tiene guardado una configuracion diferente de movimiento.
     */
    public void leerControlesJugador(String fuente, Jugador jugador) {
        ArrayList<String> listaControles = new ArrayList<>();
        if (jugador.getNumeroJugador() == 1) {
            listaControles = leerControles(fuente);
            control = listaControles.get(0).split(",");
            arriba = control[0];
            abajo = control[1];
            derecha = control[2];
            izquierda = control[3];
            jugador.setControles(control);
        } else if (jugador.getNumeroJugador() == 2) {
            listaControles = leerControles(fuente);
            control = listaControles.get(1).split(",");
            arriba = control[0];
            abajo = control[1];
            derecha = control[2];
            izquierda = control[3];
            jugador.setControles(control);
        }
    }

    /**
     * Metodo getter de String arriba
     * @return arriba: String que almacena la tecla para moverse hacia arriba.
     */
    public String getArriba() {
        return arriba;
    }

    public void setArriba(String arriba) {
        this.arriba = arriba;
    }

    /**
     * Metodo getter de String abajo
     * @return abajo: String que almacena la tecla para moverse hacia abajo.
     */
    public String getAbajo() {
        return abajo;
    }

    /**
     *Metodo setter de String abajo
     * @param abajo String que setea el movimiento hacia abajo
     */
    public void setAbajo(String abajo) {
        this.abajo = abajo;
    }

    /**
     * Metodo getter de String izquierda
     * @return izquierda: String que almacena la tecla para moverse hacia la izquierda.
     */
    public String getIzquierda() {
        return izquierda;
    }

    /**
     *Metodo setter de String Izquierda
     * @param izquierda String que setea el movimiento hacia la izquierda
     */
    public void setIzquierda(String izquierda) {
        this.izquierda = izquierda;
    }

    /**
     * Metodo getter de String derecha
     * @return derecha: String que almacena la tecla para moverse hacia la derecha.
     */
    public String getDerecha() {
        return derecha;
    }

    /**
     *Metodo setter de String Izquierda
     * @param derecha String que setea el movimiento hacia la derecha
     */
    public void setDerecha(String derecha) {
        this.derecha = derecha;
    }

    /**
     * Lista de controles.
     * @return abajo: lista que almacena las teclas para la movilidad del jugador
     */
    public String[] getControl() {
        return control;
    }

    /**
     * Lista de controles.
     * @param control lista que setea los movimientos del jugador.
     */
    public void setControl(String[] control) {
        this.control = control;
    }
    
    
    
   
    
}
