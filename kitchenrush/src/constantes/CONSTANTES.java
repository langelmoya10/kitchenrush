/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

/**
 * Clase que establece valores constantes que se utilizaran en varios aspectos a lo largo de toda la ejecucion del juego.
 * @author CHRISTIN
 */
public class CONSTANTES {
    public static final String RUTA_IMAGENES="recursos";
    public static final String RUTA_IMAGENES_PLATILLO="src//recursos//";
    public static final String JUGADOR_1_PATH="src//recursos//niñaabajo.png";
    public static final String JUGADOR_2_PATH="src//recursos//temor.png";
    public static final String CONTROLES_PATH = "src//Controles//controles.txt";
    public static final String PLATILLOS_PATH = "src//recursos//platillo.txt";
    public static final String PRODUCTOS_PATH = "src//recursos//productos.txt";
    
    public static final String TITULO = "Kitchen Rush";
    public static final double ANCHO_JUEGO = 1300;
    public static final double ALTURA_JUEGO = 800;
    public static final double POSICION_PRODUCTOSY=600;
    public static final int FILAS = 6;
    public static final int COLUMNAS = 11;
    public static final int ANCHO_PLATILLO=80;
    public static final int ALTURA_PLATILLO=80;
    public static final int ANCHO_PRODUCTO=40;
    public static final int ALTURA_PRODUCTO=40;
    public static final int ALTURA_JUGADOR=70;
    public static final int ANCHO_JUGADOR=70;
    public static final int VELOCIDAD_JUGADOR=30;
    public static final int POSICION_X_ESTACION1=0;
    public static final int POSICION_Y_ESTACION1=100;
    public static final int ALTURA_ESTACION=400;
    public static final int ANCHO_ESTACION=100;
    public static final int POSICION_X_ESTACION2=((int)CONSTANTES.ANCHO_JUEGO-CONSTANTES.ANCHO_ESTACION+10);
    public static final int POSICION_Y_ESTACION2=100;
    
    
    
}
