/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import constantes.CONSTANTES;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import kitchenrush_2p.PanelInicial;

/**
 * Clase perteneciente al modelo grafico, Muestra la opcion de ingresar un nuevo platillo junto a los productos necesarios para prepararlo.
 * @author CHRISTIN
 */
public class VistaIngresoPlatillo {
    
    private Pane gamePane;
    private Label nameDish;
    private TextField name;
    private ComboBox op1;
    private ComboBox op2;
    private ComboBox op3;
    private ComboBox op4;
    private ComboBox op5;
    private ImageView fotoP;
    private Button cargar;
    private Button ingresar;
    private Button menu; 
    private Rectangle rect;
    private VistaIngresoPlatillo aqui;
    private String ruta;
    private ArrayList<String> opciones;
    
    /**
     * Constructor el cual permitira colocar un pane (que contiene los botones, combo box, y text areas) en el
     * centro de un BorderPane
     * @param root BorderPane que se encuentra dentro de la scene de nuestra ventana juego
     */
    public VistaIngresoPlatillo(BorderPane root){
        gamePane = new Pane();        
        
        aqui = this;
        ruta = ""; rect = new Rectangle(300, 280);
        
        root.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_IMAGENES+"/fondo-juego.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+CONSTANTES.ANCHO_JUEGO+" "+(CONSTANTES.ALTURA_JUEGO)+"; "
                + "-fx-background-position: center center;");
       
        nameDish = new Label("Nombre del platillo: ");
        nameDish.setFont(Font.font("Helvetica", FontWeight.BOLD, 20));
        name = new TextField(); name.setMinWidth(225); 
        
        setCombos();
        
        ingresar = new Button("Ingresar Platillo");
        ingresar.setOnAction(new Acciones(1));
        ingresar.setStyle("-fx-background-color: POWDERBLUE");  
        ingresar.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        
        cargar = new Button("Cargar Imagen"); 
        cargar.setOnAction(new Acciones(2));
        cargar.setStyle("-fx-background-color: POWDERBLUE");  
        cargar.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        
        menu = new Button("Menu Principal");
        menu.setStyle("-fx-background-color: POWDERBLUE"); 
        menu.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        
        HBox hb = new HBox();
        hb.getChildren().addAll(nameDish, name);
        hb.setSpacing(5);
        
        HBox hb2 = new HBox();
        hb2.getChildren().addAll(cargar, ingresar, menu);
        hb2.setSpacing(5);
        
        VBox vb = new VBox();
        vb.getChildren().addAll(hb, op1, op2, op3, op4, op5, hb2);
        vb.setSpacing(10);
        vb.setLayoutX(200);
        vb.setLayoutY(200);
        
        rect.setLayoutX(750);
        rect.setLayoutY(200);
        rect.setFill(Color.TRANSPARENT); rect.setStroke(Color.BLACK);
        
        gamePane.getChildren().addAll(vb, rect);
        
        crearCentro();
        
        root.setCenter(gamePane);
    }
    
    /**
     * Metodo que setea al pane de la vista un tamaño especifico para el juego
     */
    private void crearCentro(){
        gamePane.setPrefSize(CONSTANTES.ANCHO_JUEGO, CONSTANTES.ALTURA_JUEGO);                
    }
    
    /**
     * Metodo que setea las distintas opciones de los combobox que hacen referencia a los productos de 
     * los platillos
     */
    private void setCombos(){
        op1 = new ComboBox(); op1.setPromptText("Elija un producto");
        op2 = new ComboBox(); op2.setPromptText("Elija un producto");
        op3 = new ComboBox(); op3.setPromptText("Elija un producto");
        op4 = new ComboBox(); op4.setPromptText("Elija un producto");
        op5 = new ComboBox(); op5.setPromptText("Elija un producto");
        
        op1.setStyle("-fx-background-color: POWDERBLUE");  
        op2.setStyle("-fx-background-color: POWDERBLUE");  
        op3.setStyle("-fx-background-color: POWDERBLUE");  
        op4.setStyle("-fx-background-color: POWDERBLUE");  
        op5.setStyle("-fx-background-color: POWDERBLUE");  
        
        op1.getItems().addAll(PanelInicial.nombreProducto);
        op2.getItems().addAll(PanelInicial.nombreProducto);
        op3.getItems().addAll(PanelInicial.nombreProducto);
        op4.getItems().addAll(PanelInicial.nombreProducto);
        op5.getItems().addAll(PanelInicial.nombreProducto);        
    }
    
    /**
     * Metodo que permitira subir la imagen del platillo en la vista, para luego obtener la ruta de esta
     * y poder almacenarla despues en el documento platillo.txt
     */
    private void subida(){
        gamePane.getChildren().remove(fotoP);
        FileNameExtensionFilter ext = new FileNameExtensionFilter("Archivo de imagen","jpg", "jpeg", "png");
        
        JFileChooser jfile = new JFileChooser("src/recursos");
        jfile.setFileFilter(ext);
        jfile.setDialogTitle("Abrir Imagen");
        
        int op = jfile.showOpenDialog(jfile);
        if (op == JFileChooser.APPROVE_OPTION) {
            File archivo = jfile.getSelectedFile();
            String ruta2 = archivo.getPath();
            ruta = ruta2;       
            fotoP = new ImageView();
            Image image = new Image(archivo.toURI().toString());
            fotoP.setImage(image);
            fotoP.setFitHeight(CONSTANTES.ALTURA_JUGADOR);
            fotoP.setFitWidth(CONSTANTES.ANCHO_JUGADOR);
            fotoP.setLayoutX(860);
            fotoP.setLayoutY(300);
            
            gamePane.getChildren().addAll(fotoP);            
        }
    }
    
    /**
     * Metodo que validará el ingreso del platillo al juego, en el cual se deberá cargar su imagen, darle un nombre 
     * y escoger como minimo tres productos
     */
    private void ingreso(){
        Alert alerta;        
        opciones = new ArrayList<>();
        if(op1.getSelectionModel().getSelectedItem() != null){
            opciones.add(op1.getSelectionModel().getSelectedItem().toString());
        }
        if(op2.getSelectionModel().getSelectedItem() != null){
            opciones.add(op2.getSelectionModel().getSelectedItem().toString());
        }
        if(op3.getSelectionModel().getSelectedItem() != null){
            opciones.add(op3.getSelectionModel().getSelectedItem().toString());
        }
        if(op4.getSelectionModel().getSelectedItem() != null){
            opciones.add(op4.getSelectionModel().getSelectedItem().toString());
        }
        if(op5.getSelectionModel().getSelectedItem() != null){
            opciones.add(op5.getSelectionModel().getSelectedItem().toString());
        }
        
        if(opciones.size() < 3){
            alerta = new Alert(Alert.AlertType.NONE, "Se deben escoger al menos 3 productos", ButtonType.OK);
            alerta.setTitle("ATENCIÓN: INGRESO INCORRECTO");
            alerta.showAndWait();
        } else if(name.getText().isEmpty() || ruta.isEmpty()){
            alerta = new Alert(Alert.AlertType.NONE, "Debe ingresar el nombre del producto y cargar su imagen", ButtonType.OK);
            alerta.setTitle("ATENCIÓN: INGRESO INCORRECTO");
            alerta.showAndWait();
        } else {
            
            int i = 0;           
            for (i = 0; i < PanelInicial.nombrePlatillos.size(); i++) {
                if(PanelInicial.nombrePlatillos.get(i).equalsIgnoreCase(name.getText())){
                    alerta = new Alert(Alert.AlertType.NONE, "Nombre de platillo ya existente", ButtonType.OK);
                    alerta.setTitle("ATENCIÓN: INGRESO INCORRECTO");
                    alerta.showAndWait();
                    
                    break;
                }
            }
            
            if(i == PanelInicial.nombrePlatillos.size()){
                agregar();
                opciones.clear();
                name.clear();
                op1.getSelectionModel().clearSelection();
                op2.getSelectionModel().clearSelection();
                op3.getSelectionModel().clearSelection();
                op4.getSelectionModel().clearSelection();
                op5.getSelectionModel().clearSelection();
                alerta = new Alert(Alert.AlertType.NONE, "Ingreso exitoso", ButtonType.OK);
                alerta.setTitle("ATENCIÓN: INGRESO CORRECTO");
                alerta.showAndWait();       
            }          
        }                
    }
    
    /**
     * Este método permitira agregar el platillo, la ruta de su imagen y los nombres de sus ingredientes o
     * productos al documento de texto llamado platillo.txt
     */
    public void agregar(){
        String line = "\n"+name.getText()+","+ruta;
        int cant = opciones.size();
        
        for (int i = 0; i < cant; i++) {
            line = line+","+opciones.get(i);
        }
        
        PrintWriter outputStream = null;
        try{            
            outputStream =  new PrintWriter(new FileOutputStream("src/recursos/platillo.txt",true));            
            outputStream.print(line);
        }
        catch(FileNotFoundException e){
            System.out.println("Error opening the file out.txt.");
            System.exit(0);
        }finally{
            outputStream.close();
        }
        
        PanelInicial.nombrePlatillos.add(name.getText());
        
    }

    /**
     * Metodo que retornará el boton menu principal de la vista 
     * @return Boton de regreso al menu principal
     */
    public Button getMenu() {
        return menu;
    }

    /**
     * Metodo que me permitira setear a la vista un boton menu
     * @param menu boton de la vista para regresar al menu principal
     */
    public void setMenu(Button menu) {
        this.menu = menu;
    }
    
    /**
     * Clase privada que permitira dar opciones a los botones de INGRESO y SUBIDA DE IMAGEN de la vista
     */
    private class Acciones implements EventHandler<ActionEvent> {
        
        int opcion;
        
        /**
         * Constructor de clase privada
         * @param opcion dependiendo de su valor se realizara una accion diferente
         */
        public Acciones(int opcion) {
            this.opcion = opcion;            
        }        
        
        /**
         * Metodo que permitira realizar la accion al dar clic a un boton
         * @param event evento que se efectuara dependiendo del clic dado
         */
        public void handle(ActionEvent event) {

            if (this.opcion == 1) {     
                aqui.ingreso();
            }
            if (this.opcion == 2) { 
                aqui.subida();
            }
        }

    }
    
}
