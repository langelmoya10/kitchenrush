/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import constantes.CONSTANTES;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import kitchenrush_2p.PanelInicial;

/**
 * Clase perteneciente al modelo grafico, Muestra la vista Menu (Ventana Principal) con las diversas opciones que tiene el usuario a elegir.
 * @author CHRISTIN
 */
public class VistaMenu {
    
    private VBox logo;
    private Pane juego;
    private Button iniciarJ;
    private Button ingresarR;
    private Button ingresarP;
    private Button salir;
    private VBox botones;
    private GridPane gp;
    private BorderPane border;
    private ImageView image;
    private PanelInicial panelInicial;
            
    /**
     *Constructor de la VIstaMenu
     * @param root BorderPane en donde iran los objetos de dicha vista.
     */
    public VistaMenu(BorderPane root) {
       
        gp = new GridPane();
        border = new BorderPane();
        botones=new VBox();
        logo = new VBox(70);        
        juego = new Pane();     
       
        setCenter();
        fondoPrincipal();
        setBotones();

        juego.getChildren().addAll(border,logo,botones);
        root.setCenter(juego);
    }
   
    /**
     *Metodo que me permite setear una imagen de fondo.
     */
    public void setCenter(){
        gp.setMinSize(400,200);
        gp.setVgap(1); 
        gp.setHgap(1);  
        gp.setPadding(new Insets(0, 0, 0, 0));
        for (int i=0;i<CONSTANTES.FILAS;i++){
            for(int j=0;j<CONSTANTES.COLUMNAS;j++){
                image = new ImageView(CONSTANTES.RUTA_IMAGENES+"/stone-background.jpg");    
                image.setFitHeight(150);
                image.setFitWidth(150);
                gp.add(image,j,i);
            }
        }
        border.setCenter(gp);
    }
    
    /**
     *Metodo que permite establecer imagenes y agregarlas al pane principal de inicio 
     */
    public void fondoPrincipal(){
        ImageView imagen2 = new ImageView(CONSTANTES.RUTA_IMAGENES+"/Intensamente.png");
        ImageView imagen3 = new ImageView(CONSTANTES.RUTA_IMAGENES+"/kitchen-rush.png");
        logo.getChildren().addAll(imagen3, imagen2);
        logo.setAlignment(Pos.CENTER_LEFT);
    }
    
    /**
     *Metodo para agregar los objetos botones y labels a la vista principal
     */
    public void setBotones(){
        iniciarJ = new Button("Iniciar Juego");
        ingresarR = new Button ("Ingresar Nuevos Platillos");
        ingresarP = new Button ("Ingresar Nuevos Productos");
        salir = new Button ("Salir");
        botones.getChildren().addAll(iniciarJ, ingresarR, ingresarP, salir);
        botones.setAlignment(Pos.CENTER_RIGHT);
        
        iniciarJ.setFont(Font.font("MV Boli", 28));
        iniciarJ.setStyle("-fx-background-color:rgba(254, 59, 7,0.8);-fx-background-radius:10;");

        ingresarR.setFont(Font.font("MV Boli", 28));
        ingresarR.setStyle("-fx-background-color:rgba(254, 59, 7,0.8);-fx-background-radius:10;");
        
        ingresarP.setFont(Font.font("MV Boli", 28));
        ingresarP.setStyle("-fx-background-color:rgba(254, 59, 7,0.8);-fx-background-radius:10;");
        
        salir.setFont(Font.font("MV Boli", 28));
        salir.setStyle("-fx-background-color:rgba(254, 59, 7,0.8);-fx-background-radius:10;");
        
        
        botones.setAlignment(Pos.CENTER);
        botones.setSpacing(40);
        botones.setLayoutX(750);
        botones.setLayoutY(340);
    }    
    
    public Pane getJuego() {
        return juego;
    }
    
    public void setJuego(Pane juego) {
        this.juego = juego;
    }

    public Button getSalir() {
        return salir;
    }

    public void setSalir(Button salir) {
        this.salir = salir;
    }
    
     public Button getIniciarJ() {
        return iniciarJ;
    }

    public void setIniciarJ(Button iniciarJ) {
        this.iniciarJ = iniciarJ;
    }

    public Button getIngresarR() {
        return ingresarR;
    }

    public void setIngresarR(Button ingresarR) {
        this.ingresarR = ingresarR;
    }

    public Button getIngresarP() {
        return ingresarP;
    }

    public void setIngresarP(Button ingresarP) {
        this.ingresarP = ingresarP;
    }
    
}
