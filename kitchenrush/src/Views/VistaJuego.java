/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Modelo.Jugador;
import constantes.CONSTANTES;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import Hilos.*;
import Modelo.Platillo;
import Modelo.Producto;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import javafx.animation.Animation;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import kitchenrush_2p.KitchenRush_2P;
import kitchenrush_2p.PanelInicial;

/**
 * Clase perteneciente al modelo grafico, Muestra el lugar en donde el jugador
 * puede moverse, recolectar/dejar productos hasta completar los diversos
 * productos.
 *
 * @author CHRISTIN
 */
public class VistaJuego {

    private Timeline timelineTiempo;
    public static Pane gamePane;
    public static HBox barraSuperior;
    private ImageView imagen;
    private Platillo platilloJ2;
    private Platillo platilloJ1;
    private Label numTiempoJuego;
    private Pane rootEstacion;
    private Jugador j1;
    private Jugador j2;
    private BorderPane root;
    public static Rectangle EstacionTrabajo1;
    public static Rectangle EstacionTrabajo2;
    private HiloMovimiento mj;
    private HiloProducto hp;
    private HiloProductoMesaTrabajo hpmt1;
    private HiloProductoMesaTrabajo hpmt2;
    private HiloAdministracionPlatillos hiloadministradorPlatilloJ1;
    private HiloAdministracionPlatillos hiloadministradorPlatilloJ2;

    public static ArrayList<Platillo> listaPlatillos;
    public static ArrayList<Producto> listaProductos;
    private Timeline timelineCogerObjetos;
    private Button botonPausa;
    public static HBox imagenesPlatillosJ1;
    public static HBox imagenesPlatillosJ2;

    /**
     * Constructor por defecto
     */
    public VistaJuego() {

    }

    /**
     * COnstructor de VistaJuego
     *
     * @param root BOrderPane general en donde iran todos los objetos del juego.
     */
    public VistaJuego(BorderPane root) {
        this.root = root;
        gamePane = new Pane();
        ImagenFondo(root);
        cargarEstacion(root);
        cargarBarraSuperior(root);

    }

    /**
     * Metodo que setea la imagen de fondo del pane principal
     *
     * @param root pane principal
     */
    public void ImagenFondo(BorderPane root) {
        root.setStyle("-fx-background-image: url('" + CONSTANTES.RUTA_IMAGENES + "/fondo-juego.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: " + (CONSTANTES.ANCHO_JUEGO + 100) + " " + (CONSTANTES.ALTURA_JUEGO + 100) + "; "
                + "-fx-background-position: center center;");
    }

    /**
     * Metodo que carga los objetos en la parte superior del BorderPane
     * principal
     *
     * @param root pane principal
     */
    public void cargarBarraSuperior(BorderPane root) {
        barraSuperior = new HBox();
        platilloJ1 = new Platillo();

        VBox puntajeJ1 = new VBox();
        Label lblpuntosJ1 = new Label("PUNTOS J1");
        Label puntosJ1 = new Label("0");

        imagenesPlatillosJ1 = new HBox();
        imagenesPlatillosJ1.setMinHeight(70);
        imagenesPlatillosJ1.setMaxHeight(90);

        puntajeJ1.getChildren().addAll(lblpuntosJ1, puntosJ1, imagenesPlatillosJ1);
        puntajeJ1.setSpacing(15);
        puntajeJ1.setAlignment(Pos.CENTER);
        VBox tiempoJ1 = new VBox();
        Label lblTiempoJ1 = new Label("Tiempo Jugador 1");
        Label numTiempoJ1 = new Label("");
        tiempoJ1.getChildren().addAll(lblTiempoJ1, numTiempoJ1);
        tiempoJ1.setSpacing(50);
        tiempoJ1.setAlignment(Pos.CENTER);

        VBox tiempoJuego = new VBox();
        Label lblTiempoJuego = new Label("Tiempo Juego");
        numTiempoJuego = new Label("");
        tiempoJuego.getChildren().addAll(lblTiempoJuego, numTiempoJuego);
        tiempoJuego.setSpacing(50);
        tiempoJuego.setAlignment(Pos.CENTER);

        VBox tiempoJ2 = new VBox();
        Label lblTiempoJ2 = new Label("Tiempo Jugador 2");
        Label numTiempoJ2 = new Label("");
        tiempoJ2.getChildren().addAll(lblTiempoJ2, numTiempoJ2);
        tiempoJ2.setSpacing(50);
        tiempoJ2.setAlignment(Pos.CENTER);

        VBox puntajeJ2 = new VBox();
        Label lblpuntosJ2 = new Label("PUNTOS J2");
        Label puntosJ2 = new Label("0");

        imagenesPlatillosJ2 = new HBox();
        imagenesPlatillosJ2.setMinHeight(70);
        imagenesPlatillosJ2.setMaxHeight(90);
        puntajeJ2.getChildren().addAll(lblpuntosJ2, puntosJ2, imagenesPlatillosJ2);
        puntajeJ2.setSpacing(15);
        puntajeJ2.setAlignment(Pos.CENTER);

        platilloJ2 = new Platillo();
        barraSuperior.setStyle("-fx-background-color:rgba(249, 186, 18,0.4);-fx-background-radius:10;");
        barraSuperior.setSpacing(80);
        botonPausa = new Button("PAUSA");
        botonPausa.setAlignment(Pos.CENTER);
        botonPausa.setOnAction(new Pause());

        barraSuperior.getChildren().addAll(platilloJ1.getImagenPlatillo(), puntajeJ1, tiempoJ1, tiempoJuego, tiempoJ2, puntajeJ2, platilloJ2.getImagenPlatillo(), botonPausa);
        root.setAlignment(tiempoJ1, Pos.CENTER);
        root.setTop(barraSuperior);
        setupTimeTiempoJuego();
        /*hpmt1 = new HiloProductoMesaTrabajo(barraSuperior, rootEstacion, j1, 1);
        hpmt1.start();

        hpmt2 = new HiloProductoMesaTrabajo(barraSuperior, rootEstacion, j2, 2);
        hpmt2.start();*/
        hiloadministradorPlatilloJ1 = new HiloAdministracionPlatillos(barraSuperior, rootEstacion, j1, 1);
        hiloadministradorPlatilloJ1.start();
        hiloadministradorPlatilloJ2 = new HiloAdministracionPlatillos(barraSuperior, rootEstacion, j2, 2);
        hiloadministradorPlatilloJ2.start();

    }

    /**
     * Metodo que carga los objetos de la estacion de trabajo de ambos jugadores
     * y carga los hilos del movimiendo del jugador y el hilo de la creaciond
     * aleatoria de los productos.
     *
     * @param root pane principal
     */
    public void cargarEstacion(BorderPane root) {
        rootEstacion = new Pane();
        j1 = new Jugador(1, rootEstacion);
        j2 = new Jugador(2, rootEstacion);
        Jugador.evento(j1, j2);

        EstacionTrabajo1 = new Rectangle(100, 400);
        EstacionTrabajo1.setFill(Color.BURLYWOOD);
        EstacionTrabajo1.setLayoutX(CONSTANTES.POSICION_X_ESTACION1);
        EstacionTrabajo1.setLayoutY(CONSTANTES.POSICION_Y_ESTACION1);
        EstacionTrabajo2 = new Rectangle(CONSTANTES.ANCHO_ESTACION, CONSTANTES.ALTURA_ESTACION);
        EstacionTrabajo2.setFill(Color.BURLYWOOD);
        EstacionTrabajo2.setLayoutX(CONSTANTES.POSICION_X_ESTACION2);
        EstacionTrabajo2.setLayoutY(CONSTANTES.POSICION_Y_ESTACION2);
        //System.out.println("estaciones creadas");     

        cargarProductos(CONSTANTES.PRODUCTOS_PATH);
        cargarPlatillos(CONSTANTES.PLATILLOS_PATH);
        rootEstacion.getChildren().addAll(EstacionTrabajo1, EstacionTrabajo2);
        root.setCenter(rootEstacion);

        mj = new Hilos.HiloMovimiento(j1, j2);
        mj.start();
        hp = new HiloProducto(rootEstacion);
        hp.start();
    }

    /**
     * Metodo que lee los datos de un archivo txt y los guarda en un arreglo
     *
     * @param fuente tipo String, Hace referencia a la direccion del archivo txt
     * con los datos.
     * @return Una lista de String con los datos que posee el archivo txt.
     */
    public ArrayList<String> CargarArchivo(String fuente) {
        ArrayList<String> linea = new ArrayList<>();
        BufferedReader inputStream = null;
        String line = null;
        try {
            inputStream = new BufferedReader(new FileReader(fuente));
            while ((line = inputStream.readLine()) != null) {
                linea.add(line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        } catch (IOException e) {
            System.out.println("Error al momento de leer el archivo.");
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {

            }
        }
        return linea;
    }

    /**
     * Metodo que carga los platillos desde el archivo platillos.txt hacia un
     * arreglo de Platillos. Asi al iniciar el juego se tendra un arreglo de
     * platillos y luego pueden ser llamados aleatoriamente.
     *
     * @param fuente tipo String, Hace referencia a la direccion del archivo a
     * leer la data.
     */
    public void cargarPlatillos(String fuente) {
        ArrayList<String> linea = CargarArchivo(fuente);
        listaPlatillos = new ArrayList<>();
        for (int i = 0; i < linea.size(); i++) {
            ArrayList<Producto> listaProductos_Platillo = new ArrayList();
            String[] control = linea.get(i).split(",");
            for (int j = 2; j < control.length; j++) {
                for (int k = 0; k < listaProductos.size(); k++) {
                    if (listaProductos.get(k).getNombre().equalsIgnoreCase(control[j])) {
                        listaProductos_Platillo.add(listaProductos.get(k));//se cumple que cada platillo tenga su lista de productos.       
                    }
                }
            }

            Platillo platillo = new Platillo(control[0], listaProductos_Platillo, control[1]);
            if (listaProductos_Platillo.size() == 3) {
                platillo.setPuntos(30);
            }
            if (listaProductos_Platillo.size() == 4) {
                platillo.setPuntos(40);
            }
            if (listaProductos_Platillo.size() == 5) {
                platillo.setPuntos(50);
            }
            listaPlatillos.add(platillo);
        }

    }

    /**
     * Metodo que carga los productos desde un archivo productos.txt hacia un
     * arreglo de productos. Asi al iniciar el juego se tendra un arreglo de
     * productos y luego pueden ser llamados aleatoriamente.
     *
     * @param fuente tipo Stirng, Hace referencia a la direccion de donde se
     * quiere leer la data.
     */
    public void cargarProductos(String fuente) {
        ArrayList<String> linea = CargarArchivo(fuente);
        listaProductos = new ArrayList<>();

        for (int i = 0; i < linea.size(); i++) {
            String[] control = linea.get(i).split(",");
            Producto producto = new Producto(control[0], control[1]);
            listaProductos.add(producto);
        }
    }

    /**
     * Metodo que Agrega una imagen a un ImageView
     *
     * @param direccion tipo String, Hace referencia a la ubicacion de la
     * imagen.
     * @param psx tipo double, Hace referencia a la altura de la ImageView
     * @param psy tipo double, Hace referencia a la anchira de la ImageView
     * @return Imageview
     */
    public ImageView llamarImagen(String direccion, double psx, double psy) {
        InputStream is;
        try {
            is = Files.newInputStream(Paths.get(direccion));
            Image image = new Image(is);
            imagen = new ImageView(image);
            imagen.setFitHeight(psx);
            imagen.setFitWidth(psy);
        } catch (IOException ex) {
            System.out.println("\nNo encontrado el jugador");
        }
        return (imagen);
    }

    /**
     * InnerClass que administra el tiempo total del juego. 120 segundos=2
     * minutos
     */
    private class TimeHandlerTiempoJuego implements EventHandler<ActionEvent> {

        int t = 119;

        public void handle(ActionEvent event) {

            numTiempoJuego.setText(String.valueOf(t));

            if (t == 0) {
                numTiempoJuego.setText("FIN DEL JUEGO");
                root.getChildren().remove(barraSuperior);

                timelineTiempo.stop();
                //hpmt1.getTimelineTiempo().stop();
                //hpmt2.getTimelineTiempo().stop();
                //hpmt1.setBandera(false);
                //hpmt2.setBandera(false);
                hiloadministradorPlatilloJ1.getHpmt1().getTimelineTiempo().stop();
                hiloadministradorPlatilloJ2.getHpmt2().getTimelineTiempo().stop();
                hiloadministradorPlatilloJ1.getHpmt1().setBandera(false);
                hiloadministradorPlatilloJ2.getHpmt2().setBandera(false);
                hp.setBandera(false);
                mj.setBandera(false);

                hiloadministradorPlatilloJ2.setBandera(false);
                hiloadministradorPlatilloJ1.setBandera(false);

                vistaGanador ganador = new vistaGanador(root);
                ganador.ganador(j1, j2);
            } else {
                t = t - 1;
            }
        }
    }

    /**
     * Metodo que asemeja a un hilo el cual hara ejecutar cierto codigo
     * repetidas veces.
     */
    public void setupTimeTiempoJuego() {
        KeyFrame kf = new KeyFrame(javafx.util.Duration.seconds(1), new TimeHandlerTiempoJuego());
        timelineTiempo = new Timeline(kf);
        timelineTiempo.setCycleCount(123);//123
        timelineTiempo.play();
    }

    /**
     * Metodo que asemeja a un hilo el cual tiene la funcion de pausar el juego
     */
    private class Pause implements EventHandler<ActionEvent> {

        private Alert alerta = null;

        public void handle(ActionEvent event) {
            try {
                if ((event.getSource() == botonPausa) && (timelineTiempo.getStatus() != Animation.Status.STOPPED) && (hiloadministradorPlatilloJ1.getHpmt1().getTimelineTiempo().getStatus() != Animation.Status.STOPPED) && (hiloadministradorPlatilloJ2.getHpmt2().getTimelineTiempo().getStatus() != Animation.Status.STOPPED)) {
                    timelineTiempo.stop();
                    hiloadministradorPlatilloJ1.getHpmt1().getTimelineTiempo().stop();
                    hiloadministradorPlatilloJ2.getHpmt2().getTimelineTiempo().stop();
                    hiloadministradorPlatilloJ1.getHpmt1().suspend();
                    hiloadministradorPlatilloJ2.getHpmt2().suspend();
                    hiloadministradorPlatilloJ2.suspend();
                    hiloadministradorPlatilloJ1.suspend();
                    hp.suspend();
                    mj.suspend();

                    alerta = new Alert(Alert.AlertType.NONE, "Click en OK para renaudar el juego.\nClick en FINISH para regresar al menú principal", ButtonType.OK, ButtonType.FINISH);
                    alerta.setTitle("ATENCIÓN: JUEGO PAUSADO");
                    alerta.showAndWait();

                    if (alerta.getResult() == ButtonType.OK) {
                        timelineTiempo.play();
                        hiloadministradorPlatilloJ1.getHpmt1().getTimelineTiempo().play();
                        hiloadministradorPlatilloJ2.getHpmt2().getTimelineTiempo().play();
                        hiloadministradorPlatilloJ1.getHpmt1().resume();
                        hiloadministradorPlatilloJ2.getHpmt2().resume();
                        hiloadministradorPlatilloJ1.resume();
                        hiloadministradorPlatilloJ2.resume();
                        hp.resume();
                        mj.resume();
                        

                    } else if (alerta.getResult() == ButtonType.FINISH) {
                        timelineTiempo.stop();
                        hiloadministradorPlatilloJ1.getHpmt1().getTimelineTiempo().stop();
                        hiloadministradorPlatilloJ2.getHpmt2().getTimelineTiempo().stop();
                        hiloadministradorPlatilloJ1.getHpmt1().setBandera(false);
                        hiloadministradorPlatilloJ2.getHpmt2().setBandera(false);
                        //hiloadministradorPlatilloJ1.stop();
                        //hiloadministradorPlatilloJ2.stop();
                        hp.setBandera(false);
                        mj.setBandera(false);
                       
                        hiloadministradorPlatilloJ1.setBandera(false);

                        hiloadministradorPlatilloJ2.setBandera(false);
                        KitchenRush_2P kr = new KitchenRush_2P();
                        PanelInicial.productoEnMesa1.clear();
                        PanelInicial.productoEnMesa2.clear();
                        PanelInicial.productosActivos.clear();
                        PanelInicial.nombrePlatillos.clear();
                        PanelInicial.nombreProducto.clear();

                        KitchenRush_2P.scene = new Scene(new PanelInicial().getRoot(), KitchenRush_2P.primaryStage.getScene().getWidth(), KitchenRush_2P.primaryStage.getScene().getHeight());
                        KitchenRush_2P.primaryStage.setScene(KitchenRush_2P.scene);
                        kr.start(KitchenRush_2P.primaryStage);
                    }
                }
            } catch (Exception ex) {

            }

        }
    }

    /**
     * Metodo getter de gamePause
     *
     * @return Pane
     */
    public static Pane getGamePane() {
        return gamePane;
    }

    /**
     * Metodo setter de gamepane
     *
     * @param gamePane nuevo valor para gamePane
     */
    public static void setGamePane(Pane gamePane) {
        VistaJuego.gamePane = gamePane;
    }

    /**
     * Metodo getter de barraSuperior
     *
     * @return HBox
     */
    public HBox getBarraSuperior() {
        return barraSuperior;
    }

    /**
     * Metodo setter de barraSuperior
     *
     * @param barraSuperior nuevo valor para barraSuperior
     */
    public void setBarraSuperior(HBox barraSuperior) {
        this.barraSuperior = barraSuperior;
    }

    /**
     * Metodo getter de J1 que hace referencia al jugador 1
     *
     * @return Jugador
     */
    public Jugador getJ1() {
        return j1;
    }

    /**
     * Metodo setter de j1 que hace referencia al jugador1
     *
     * @param j1 Jugador
     */
    public void setJ1(Jugador j1) {
        this.j1 = j1;
    }

    /**
     * Metodo getter de J2 que hace referencia al jugador 2
     *
     * @return Jugador
     */
    public Jugador getJ2() {
        return j2;
    }

    /**
     * Metodo setter de j2 que hace referencia al jugador2
     *
     * @param j2 Jugador
     */
    public void setJ2(Jugador j2) {
        this.j2 = j2;
    }

    /**
     * Metodo getter de EstacionTrabajo1
     *
     * @return Rectangle
     */
    public Rectangle getEstacionTrabajo1() {
        return EstacionTrabajo1;
    }

    /**
     * Metodo setter de EstacionTrabajo1
     *
     * @param EstacionTrabajo1 nuevo rectangulo
     */
    public void setEstacionTrabajo1(Rectangle EstacionTrabajo1) {
        this.EstacionTrabajo1 = EstacionTrabajo1;
    }

    /**
     * Metodo getter de EstacionTrabajo2
     *
     * @return Rectangle
     */
    public Rectangle getEstacionTrabajo2() {
        return EstacionTrabajo2;
    }

    /**
     * Metodo setter de EstacionTrabajo2
     *
     * @param EstacionTrabajo2 nuevo rectangulo.
     */
    public void setEstacionTrabajo2(Rectangle EstacionTrabajo2) {
        this.EstacionTrabajo2 = EstacionTrabajo2;
    }

}
