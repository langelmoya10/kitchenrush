/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import constantes.CONSTANTES;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import kitchenrush_2p.PanelInicial;
/**
 * Clase perteneciente al modelo grafico, Muestra la opcion de ingresar un nuevo producto junto a su imagen.
 * @author CHRISTIN
 */
public class VistaIngresoProductos {
    
    private Pane gamePane;
    private TextField name;
    private Label nameProduct;
    private Button cargar;
    private Button ingresar;
    private Button menu;
    private Rectangle rect;
    private VistaIngresoProductos aqui;
    private ImageView fotoP;
    private String ruta;
    
    /**
     * Constructor el cual permitira colocar un pane (que contiene los botones y text areas) en el
     * centro de un BorderPane
     * @param root BorderPane que se encuentra dentro de la scene de nuestra ventana juego
     */
    public VistaIngresoProductos(BorderPane root){
        aqui = this;
        gamePane = new Pane();
        ruta = ""; rect = new Rectangle(300, 280);
        root.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_IMAGENES+"/fondo-juego.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+CONSTANTES.ANCHO_JUEGO+" "+(CONSTANTES.ALTURA_JUEGO)+"; "
                + "-fx-background-position: center center;");
        
        nameProduct = new Label("Nombre del producto: "); 
        nameProduct.setFont(Font.font("Helvetica", FontWeight.BOLD, 20));
        name = new TextField(); name.setMinWidth(250); 
        
        HBox hb = new HBox();
        hb.getChildren().addAll(nameProduct, name);
        hb.setSpacing(5);
        
        ingresar = new Button("Ingresar Producto");
        ingresar.setOnAction(new Acciones(2));
        ingresar.setStyle("-fx-background-color: POWDERBLUE");
        ingresar.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        
        cargar = new Button("Cargar Imagen");
        cargar.setOnAction(new Acciones(1));     
        cargar.setStyle("-fx-background-color: POWDERBLUE");
        cargar.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        
        menu = new Button("Menu Principal");
        menu.setStyle("-fx-background-color: POWDERBLUE");
        menu.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        
        VBox vb = new VBox();
        vb.getChildren().addAll(hb,cargar, ingresar, menu);
        vb.setSpacing(20);
        vb.setLayoutX(200);
        vb.setLayoutY(200);
        
        rect.setLayoutX(750);
        rect.setLayoutY(200);
        rect.setFill(Color.TRANSPARENT); rect.setStroke(Color.BLACK);
        
        gamePane.getChildren().addAll(vb, rect);
        
        crearCentro();      
             
        root.setCenter(gamePane);
    }
    
    /**
     * Metodo que permitira subir la imagen del producto en la vista, para luego obtener la ruta de este
     * y poder almacenarla despues en el documento prodcuto.txt
     */
    private void subida(){
        gamePane.getChildren().remove(fotoP);
        FileNameExtensionFilter ext = new FileNameExtensionFilter("Archivo de imagen","jpg", "jpeg", "png");
        
        JFileChooser jfile = new JFileChooser("src/recursos");
        jfile.setFileFilter(ext);
        jfile.setDialogTitle("Abrir Imagen");
        
        int op = jfile.showOpenDialog(jfile);
        if (op == JFileChooser.APPROVE_OPTION) {
            File archivo = jfile.getSelectedFile();
            String ruta2 = archivo.getPath();
            ruta = ruta2;       

            Image image = new Image(archivo.toURI().toString());
            fotoP = new ImageView(image);
            fotoP.setFitHeight(CONSTANTES.ALTURA_JUGADOR);
            fotoP.setFitWidth(CONSTANTES.ANCHO_JUGADOR);
            fotoP.setLayoutX(860);
            fotoP.setLayoutY(300);

            gamePane.getChildren().addAll(fotoP);
            
        }
    }
    
    /**
     * Metodo que validará el ingreso del producto o ingrediente al juego, en el cual se deberá cargar su imagen 
     * respectiva y darle un nombre.
     */
    private void ingreso(){
        
        Alert alerta;        
        if(name.getText().isEmpty() || ruta.isEmpty()){
            alerta = new Alert(Alert.AlertType.NONE, "Debe ingresar el nombre del producto y cargar su imagen", ButtonType.OK);
            alerta.setTitle("ATENCIÓN: INGRESO INCORRECTO");
            alerta.showAndWait();
        } else {
            int i = 0;           
            for (i = 0; i < PanelInicial.nombreProducto.size(); i++) {
                if(PanelInicial.nombreProducto.get(i).equalsIgnoreCase(name.getText())){
                    alerta = new Alert(Alert.AlertType.NONE, "Nombre de producto ya existente", ButtonType.OK);
                    alerta.setTitle("ATENCIÓN: INGRESO INCORRECTO");
                    alerta.showAndWait();
                    
                    break;
                }
            }
            
            if(i == PanelInicial.nombreProducto.size()){
                agregar();
                name.clear();
                alerta = new Alert(Alert.AlertType.NONE, "Ingreso exitoso", ButtonType.OK);
                alerta.setTitle("ATENCIÓN: INGRESO CORRECTO");
                alerta.showAndWait();     
            }                   
        }       
    }
    
    /**
     * Este método permitira agregar el producto y la ruta de su imagen al documento de texto llamado 
     * productos.txt
     */
    private void agregar(){
                
        PrintWriter outputStream = null;
        try{            
            outputStream =  new PrintWriter(new FileOutputStream("src/recursos/productos.txt",true));
            String line = "\n"+name.getText()+","+ruta;
            outputStream.print(line);
        }
        catch(FileNotFoundException e){
            System.out.println("Error opening the file out.txt.");
            System.exit(0);
        }finally{
            outputStream.close();
        }       
        
        PanelInicial.nombreMap.put(name.getText(), ruta);
        PanelInicial.nombreProducto.add(name.getText());
        
    }
    
    /**
     * Metodo que setea al pane de la vista un tamaño especifico para el juego
     */
    private void crearCentro(){
        gamePane.setPrefSize(CONSTANTES.ANCHO_JUEGO, CONSTANTES.ALTURA_JUEGO);                
    }

    
    /**
     * Metodo que permitira obtener el boton que sirve para cargar la imagen del producto
     * @return boton para cargar la imagen
     */
    public Button getCargar() {
        return cargar;
    }

    /**
     * Metodo que me permitira setear a la vista un boton para cargar una imagen
     * @param cargar boton para vargar una imagen en la vista
     */
    public void setCargar(Button cargar) {
        this.cargar = cargar;
    }

    /**
     * Metodo que permitira obtener el boton que sirve para ingresar un producto
     * @return boton para ingresar productos al juego
     */
    public Button getIngresar() {
        return ingresar;
    }

    /**
     * Metodo que me permitira setear a la vista un boton para ingresar un nuevo producto al juego
     * @param ingresar boton que permite ingresar productos
     */
    public void setIngresar(Button ingresar) {
        this.ingresar = ingresar;
    }

    /**
     * Metodo que retornará el boton menu principal de la vista 
     * @return Boton de regreso al menu principal
     */
    public Button getMenu() {
        return menu;
    }

    /**
     * Metodo que me permitira setear a la vista un boton menu
     * @param menu boton de la vista para regresar al menu principal
     */
    public void setMenu(Button menu) {
        this.menu = menu;
    }
    
    /**
     * Clase privada que permitira dar opciones a los botones de INGRESO y SUBIDA DE IMAGEN de la vista
     */
    private class Acciones implements EventHandler<ActionEvent> {
        
        int opcion;
              
        /**
         * Constructor de clase privada
         * @param opcion dependiendo de su valor se realizara una accion diferente
         */
        public Acciones(int opcion) {
            this.opcion = opcion;            
        }        
        
        /**
         * Metodo que permitira realizar la accion al dar clic a un boton
         * @param event evento que se efectuara dependiendo del clic dado
         */
        public void handle(ActionEvent event) {

            if (this.opcion == 1) {                
                aqui.subida();
            }
            if (this.opcion == 2) {                
                aqui.ingreso();
            }
        }

    }
    
}


