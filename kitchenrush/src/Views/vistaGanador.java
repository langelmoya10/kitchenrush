/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Modelo.Jugador;
import constantes.CONSTANTES;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import kitchenrush_2p.KitchenRush_2P;
import kitchenrush_2p.PanelInicial;

/**
 * Me
 * @author CHRISTIN
 */
public class vistaGanador {
    
    private Pane gamePane;
    private Label res;
    private Label l1;
    private Label l2;
    
    /**
     * Constructor el cual permitira colocar un pane que mostrará los resultados del juego en el
     * centro de un BorderPane
     * @param root , es el pane en donde estaran ubicados los objetos en la vistaGanador
     */
    public vistaGanador(BorderPane root){
        gamePane = new Pane();        
        
        root.setStyle("-fx-background-image: url('"+CONSTANTES.RUTA_IMAGENES+"/fondo-juego.jpg');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+CONSTANTES.ANCHO_JUEGO+" "+(CONSTANTES.ALTURA_JUEGO)+"; "
                + "-fx-background-position: center center;");            
        
        crearCentro();
        
        root.setCenter(gamePane);
    }
    
    /**
     *Metodo que setea al pane de la vista un tamaño especifico para el juego 
     */
    private void crearCentro(){
        gamePane.setPrefSize(CONSTANTES.ANCHO_JUEGO, CONSTANTES.ALTURA_JUEGO);                
    }
    
    /**
     * Metodo que mostrara cual de los dos jugadores gano la partida, o si termino en un empate
     * @param j1 jugador #1 en el pane
     * @param j2 jugador #2 en el pane
     */
    public void ganador(Jugador j1, Jugador j2){
        
        String resultado = "";
        
        if(j1.getPuntaje() > j2.getPuntaje()){
            resultado = "El ganador es el JUGADOR 1";
        } else if(j2.getPuntaje() > j1.getPuntaje()){
            resultado = "El ganador es el JUGADOR 2";
        } else if(j1.getPuntaje() == j2.getPuntaje()){
            resultado = "EMPATE";
        }        
        res = new Label(resultado);
        res.setFont(Font.font("MV Boli", FontWeight.BOLD, 28));
        l1 = new Label("JUGADOR 1:                               "+ j1.getPuntaje());
        l1.setFont(Font.font("Helvetica", FontWeight.BOLD, 20));
        l2 = new Label("JUGADOR 2:                               " + j2.getPuntaje());
        l2.setFont(Font.font("Helvetica", FontWeight.BOLD, 20));

        Button menu = new Button("Menu Principal");
        menu.setStyle("-fx-background-color: POWDERBLUE");
        menu.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
        menu.setOnAction(new Acciones(1));
        menu.setAlignment(Pos.CENTER);
        
        VBox vb = new VBox();
        vb.getChildren().addAll(res,l1, VistaJuego.imagenesPlatillosJ1,l2, VistaJuego.imagenesPlatillosJ2, menu);
        vb.setSpacing(30);
        vb.setLayoutX(450);
        vb.setLayoutY(230);
        vb.setAlignment(Pos.CENTER);
        
        gamePane.getChildren().add(vb);
                
    }
    
    /**
     * Clase privada que permitira dar un evento al boton que permite el regreso al menu principal
     */
     private class Acciones implements EventHandler<ActionEvent> {

        int opcion;

        /**
         * Constructor de clase privada
         * @param opcion dependiendo de su valor se realizara una accion diferente
         */
        public Acciones(int opcion) {
            this.opcion = opcion;
        }

        /**
         * Metodo que permitira realizar la accion al dar clic al boton para regresar al menu principal
         * @param event evento que se efectuara al dar clic al boton
         */
        public void handle(ActionEvent event) {

            if (this.opcion == 1) {
                KitchenRush_2P kr = new KitchenRush_2P();
                PanelInicial.productoEnMesa1.clear();
                PanelInicial.productoEnMesa2.clear();
                PanelInicial.productosActivos.clear();
                PanelInicial.nombrePlatillos.clear();
                PanelInicial.nombreProducto.clear();

                KitchenRush_2P.scene = new Scene(new PanelInicial().getRoot(), KitchenRush_2P.primaryStage.getScene().getWidth(), KitchenRush_2P.primaryStage.getScene().getHeight());
                KitchenRush_2P.primaryStage.setScene(KitchenRush_2P.scene);

                kr.start(KitchenRush_2P.primaryStage);
            }
        }

    }
    
    
}
