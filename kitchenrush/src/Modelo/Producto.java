/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import constantes.*;
import javafx.scene.layout.Pane;
import kitchenrush_2p.PanelInicial;

/**
 * Clase que hace referencia a  un objeto Producto
 * @author CHRISTIN
 */
public class Producto {

    private String nombre;
    private ImageView imagen;
    private String path = "";
    private Pane root;
    private PanelInicial pI;
 
    /**
     *Constructor por defecto
     */
    public Producto() {
    }
    
    /**
     *Constructor de Producto
     * @param nombre tipo String, hace referencia al nombre del producto
     * @param path tipo String, hace referencia a la direccion de la imagen del producto
     */
    public Producto(String nombre, String path) {
        this.nombre = nombre;
        this.path = path;
    }

    /**
     * COnstuctor de Producto
     * @param name tipo String, hace referencia al nombre del producto
     * @param path tipo String, hace referencia a la direccion de la imagen del producto
     * @param root tipo Pane, hace referencia al Pane en donde estara el producto en el juego
     */
    public Producto(String name, String path, Pane root) {
        this.nombre = name;
        this.path = path;
        imagen = llamarImagen(path);
        root.getChildren().add(imagen);
    }

     /**
     * Metodo que Crea un objeto Imageview y lo setea en un pane.
     * @param direccion Hace referencia al path en donde esta alojada la imagen.
     * @return ImageView que hace referencia a una imagen.
     */
    public ImageView llamarImagen(String direccion) {
        InputStream is;
        try {
            is = Files.newInputStream(Paths.get(direccion));
            int posX = (int) (Math.random() * CONSTANTES.ANCHO_JUEGO - 350) + 350;
            int posY = (int) CONSTANTES.POSICION_PRODUCTOSY;
            for (int i = 0; i < pI.productosActivos.size(); i++) {
                while (pI.productosActivos.get(i).getImagen().getBoundsInParent().intersects(posX, posY, 40, 40)) {
                    posX = (int) (Math.random() * CONSTANTES.ANCHO_JUEGO - 420) + 350;
                    posY = (int) CONSTANTES.POSICION_PRODUCTOSY;
                    i=0;
                }
            }
            Image image = new Image(is, CONSTANTES.ANCHO_PRODUCTO, CONSTANTES.ALTURA_PRODUCTO, false, true);
            imagen = new ImageView(image);
            imagen.setFitHeight(CONSTANTES.ANCHO_PRODUCTO);
            imagen.setFitWidth(CONSTANTES.ALTURA_PRODUCTO);
            imagen.setLayoutX(posX);
            imagen.setLayoutY(posY);
        } catch (IOException ex) {
            System.out.println("\nNo se encuentra a la imagen");
        }
        return (imagen);
    }

    /**
     * Metodo getter de nombre
     * @return tipo String, Hace referencia al nombre del producto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo setter de nombre
     * @param nombre tipo String, Nuevo valor para el nombre del producto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo getter de la imagen
     * @return tipo ImageView, Hace referencia a la imagen del producto
     */
    public ImageView getImagen() {
        return imagen;
    }

    /**
     *Metodo setter de la imagen
     * @param imagen tipo ImageView, nuevo Imaview para el producto
     */
    public void setImagen(ImageView imagen) {
        this.imagen = imagen;
    }

    /**
     * MEtodo getter del path
     * @return tipo String, Hace referencia a la direccion de la imagen del producto
     */
    public String getPath() {
        return path;
    }

    /**
     *Metodo Setter del path
     * @param path tipo String, nueva direccion de la imagen del producto
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Metodo getter del root
     * @return tipo Pane, Hace referencia al pane en donde sera ubicado el producto
     */
    public Pane getRoot() {
        return root;
    }

    /**
     * Metodo setter del root
     * @param root tipo Pane, Hace referencia al Pane en donde sera ubicado el producto
     */ 
    public void setRoot(Pane root) {
        this.root = root;
    }

  /**
     * Metodo ToString que permite imprimir todos loa tributos de cada Producto
     * @return Una cadena de estados correspondientes a los atributos del Producto 
     */
    @Override
    public String toString() {
        return "Producto{" + "name=" + nombre + ", imagen=" + imagen + ", path=" + path + ", root=" + root + '}';
    }

}
