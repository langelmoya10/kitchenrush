/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controles.Controles;
import constantes.CONSTANTES;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import kitchenrush_2p.*;
/**
 * Clase que hace referencia al objeto jugador que sera el encargado de la interaccion del jugador  en el juego.
 * @author CHRISTIN
 */
public class Jugador {
    private ImageView imagenJugador;
    private String path = "";
    private Producto producto;
    private int puntaje;
    private int numeroJugador;
    private Pane root;
    public static ArrayList <String> teclasPresionadas = new ArrayList<String>();
    private String[] controles;
    
    /**
     *Constructor por defecto
     */
    public Jugador(){
       
    }
    
    
    /**
     *Constructor de Jugador
     * @param numeroJugador Hace referencia al id que tendra el jugador (1 o 2)
     * @param root Hace referencia al Pane en donde el jugador estara ubicado.
     */
    public Jugador(int numeroJugador, Pane root) {
        this.numeroJugador = numeroJugador;
        this.root = root;
        puntaje = 0;
        switch (numeroJugador) {
            case 1:
                path =CONSTANTES.JUGADOR_1_PATH;
                break;
            case 2:
                path = CONSTANTES.JUGADOR_2_PATH;
                break;
        }
        Controles control = new Controles();
        control.leerControlesJugador(CONSTANTES.CONTROLES_PATH, this); 
        llamarImagen(path);
        this.getRoot().getChildren().add(imagenJugador);
        producto=null;    }
    
    /**
     *Metodo que Crea un objeto Imageview y lo setea en un pane.
     * @param direccion Hace referencia al path en donde esta alojada la imagen.
     * @return ImageView que hace referencia a una imagen.
     */
    public ImageView llamarImagen(String direccion) {
        InputStream is;
        try {
            is = Files.newInputStream(Paths.get(direccion));
            Image image = new Image(is, CONSTANTES.ANCHO_JUGADOR, CONSTANTES.ALTURA_JUGADOR, false, true);
            imagenJugador = new ImageView(image);
            imagenJugador.setFitHeight(CONSTANTES.ALTURA_JUGADOR);
            imagenJugador.setFitWidth(CONSTANTES.ANCHO_JUGADOR);
            if (direccion.equalsIgnoreCase(CONSTANTES.JUGADOR_1_PATH)) {
                imagenJugador.setLayoutX(200);
                imagenJugador.setLayoutY(20);
            } else {
                imagenJugador.setLayoutX(1000);
                imagenJugador.setLayoutY(20);
            }
        } catch (IOException ex) {
            System.out.println("\nNo se encuentra al jugador");
        }
        return (imagenJugador);
    } 
    
    /**
     *Metodo que administra el evento de presionar/levantar una tecla para poder efectuar el movimiento
     * de cada jugador.
     * @param j1 Jugador 1
     * @param j2 Jugador 2
     */
    public static void evento(Jugador j1, Jugador j2){
       KitchenRush_2P.scene.setOnKeyPressed(new Jugador().new AgregarTeclas());
       KitchenRush_2P.scene.setOnKeyReleased(new Jugador().new QuitarTeclas());
    }
    
    /**
     * Inner Class que implementa a la clase EventHandler para manejar eventos KeyEvent para agregar en un Arreglo de
     * teclas y asi saber que se quiere mover a un jugador.
     */
    private class AgregarTeclas implements EventHandler<KeyEvent> {
        @Override
        public void handle(KeyEvent event) {
            KeyCode tecla = event.getCode();
            if (!teclasPresionadas.contains(tecla.toString())) {
                teclasPresionadas.add(tecla.toString());
            }

        }
    }
    
    /**
     * Inner Class que implementa a la clase EventHandler para manejar eventos KeyEvent para quitar de en un Arreglo las
     * teclas y asi saber que se quiere dejar de mover a un jugador.
     */
    private class QuitarTeclas implements EventHandler<KeyEvent> {  
        @Override
        public void handle(KeyEvent event) {
            KeyCode tecla = event.getCode();
            if(Jugador.teclasPresionadas.contains(tecla.toString())){
                Jugador.teclasPresionadas.remove(Jugador.teclasPresionadas.indexOf(tecla.toString()));
            }
        }
    }

    /**
     *Metodo getter de la imagenJugador
     * @return Node, un objeto de tipo ImageView
     */
    public Node getJugador(){
        return imagenJugador;
    }

    /**
     *Metodo getter del producto que debe de tener cada jugador
     * @return producto, un producto que debe de tener cada jugador.
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     *Metodo setter del producto
     * @param producto nuevo producto a guardar en el jugador.
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     *Metodo getter de puntaje
     * @return puntaje, Hace referencia al puntaje que ira ganando el jugador al completar los platillos
     */
    public int getPuntaje() {
        return puntaje;
    }

    /**
     *Metodo setter de puntaje
     * @param puntaje nuevo puntaje que tendra el jugador
     */
    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
    
    /**
     * Metodo getter de posicionX.
     * @return posicionX, Hace referencia a la posicion en X de la imagen que tendra el jugador.
     */
    public double getPosicionX(){
        return imagenJugador.getLayoutX();
    }
    
    /**
     * Metodo getter de posicionY.
     * @return posicionY, Hace referencia a la posicion en Y de la imagen que tendra el jugador.
     */
    public double getPosicionY(){
        return imagenJugador.getLayoutY();
    }

    /**
     * Metodo getter de imagenJugador
     * @return imagenJugador, hace referencia a la imagen del jugador que sera mostrado en el juego. 
     */
    public ImageView getImagenJugador() {
        return imagenJugador;
    }

    /**
     *Metodo setter de imagenJugador
     * @param imagenJugador nueva imagen que tendra el jugador
     */
    public void setImagenJugador(ImageView imagenJugador) {
        this.imagenJugador = imagenJugador;
    }

    /**
     * Metodo getter de controles.
     * @return controles, un arreglo que String que hace referencia a las teclas que sirven para controlar a cada jugador
     */
    public String[] getControles() {
        return controles;
    }

    /**
     * Metodo setter de controles
     * @param controles nueva lista que tendra las teclas validas para el movimiento
     */
    public void setControles(String[] controles) {
        this.controles = controles;
    }

    /**
     *metodo getter de NumeroJugador
     * @return numeroJugador, Hace referencia al id del jugador. Jugador 1 o Jugador 2
     */
    public int getNumeroJugador() {
        return numeroJugador;
    }

    /**
     * Metodo Setter del numeroJugador
     * @param numeroJugador nuevo identificador del jugador.
     */
    public void setNumeroJugador(int numeroJugador) {
        this.numeroJugador = numeroJugador;
    }

    /**
     *Metodo getter del Pane en donde esta ubicado el Jugador
     * @return root que hace referencia al pane en donde esta ubicado el jugador
     */
    public Pane getRoot() {
        return root;
    }

    /**
     * Metodo setter del Pane en donde estara el Jugador. 
     * @param root Actualiza el Pane en donde esta ubicado el jugador
     */
    public void setRoot(Pane root) {
        this.root = root;
    }
    
    
    /**
     *Metodo ToString que permite imprimir todos los atibutos de cada jugador.
     * @return Una cadena de estados correspondientes a los atributos del jugador
     */
    @Override
    public String toString() {
        return "Jugador{" + "imagenJugador=" + imagenJugador + ", path=" + path + ", producto=" + producto + ", puntaje=" + puntaje + ", numeroJugador=" + numeroJugador + ", root=" + root + ", posX=" + ", controles=" + controles + '}';
    }
    
    
}
