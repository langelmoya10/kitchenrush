/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import constantes.CONSTANTES;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Clase que hace referencia a los objetos  platillos
 * @author CHRISTIN
 */
public class Platillo {
    private String nombre;
    private ArrayList<Producto> listaProductos;
    private String path;
    private int puntos;
    private ImageView imagen;
    private ImageView imagenPlatillo;
    
    /**
     *Metodo constructor del Platillo.
     * @param nombre tipo String, Hace referencia al nombre del platillo
     */
    public Platillo(String nombre) {
        this.path = "";
        this.nombre = nombre;
    }

    /**
     * Metodo constructor del platillo
     * @param nombre tipo String, Hace referencia al nombre del platillo
     * @param listaProductos tipo ArrayList de Productos, Hace referencia a una lista de productos que necesita para ser preparado
     * @param path tipo String, Hace referencia a la ubicacion de la imagen del platillo
     */
    public Platillo(String nombre, ArrayList<Producto> listaProductos,String path) {
        this.path = "";
        this.nombre = nombre;
        this.listaProductos = listaProductos;
        this.path=path;
        imagenPlatillo=llamarImagen(path);
    }

    /**
     * Metodo constructor del platillo
     * @param nombre tipo String, Hace referencia al nombre del platillo
     * @param listaProductos tipo ArrayList de Productos , Hace referencia a una lista de productos que necesita para ser preparado
     * @param path tipo String, Hace referencia a la ubicacion de la imagen del platillo
     * @param puntos tipo int, Hace referencia a los puntos que tendra dicho platillo
     */
    public Platillo(String nombre, ArrayList<Producto> listaProductos,String path,int puntos) {
        this.path = "";
        this.nombre = nombre;
        this.listaProductos = listaProductos;
        this.path=path;
        imagenPlatillo=llamarImagen(path);
        this.puntos=puntos;
    }
     
    /**
     *Constructor por defecto
     */
    public Platillo() {
        this.path = "";
            imagenPlatillo = new ImageView();
            imagenPlatillo.setFitHeight(CONSTANTES.ANCHO_PLATILLO);
            imagenPlatillo.setFitWidth(CONSTANTES.ANCHO_PLATILLO);    }
    
    /**
     * Metodo que Crea un objeto Imageview y lo setea en un pane.
     * @param direccion Hace referencia al path en donde esta alojada la imagen.
     * @return ImageView que hace referencia a una imagen.
     */
    public ImageView llamarImagen(String direccion) {
        InputStream is;
        try {
            is = Files.newInputStream(Paths.get(direccion));
            Image image = new Image(is, CONSTANTES.ANCHO_PLATILLO, CONSTANTES.ALTURA_PLATILLO, false, true);
            imagen = new ImageView(image);
            imagen.setFitHeight(CONSTANTES.ANCHO_PLATILLO);
            imagen.setFitWidth(CONSTANTES.ANCHO_PLATILLO);      
        } catch (IOException ex) {
            System.out.println("\nNo se encuentra al jugador");
        }
        return (imagen);
    }
   
    /**
     *Metodo getter del nombre del platillo
     * @return nombre de tipo String, Hace referencia al nombre del platillo
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo Setter del nombre del platillo
     * @param nombre tipo String, Nuevo nombre que debe tener el platillo
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *Metodo getter de la lista de productos que necesita el platillo.
     * @return listaProductos de tipo ArrayList de Producuctos, Hace referencia a una lista de productos que necesita para ser preparado
     */
    public ArrayList<Producto> getListaProductos() {
        return listaProductos;
    }

    /**
     *Metodo setter de la lista de productos que necesita el platillo
     * @param listaProductos tipo ArrayList de Productos, Nueva Lista de productos para el platillo
     */
    public void setListaProductos(ArrayList<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    /**
     *Metodo getter del la ruta de la imagen del Platillo
     * @return path de tipo String,Hace referencia a la ubicacion de la imagen del platillo
     */
    public String getPath() {
        return path;
    }

    /**
     *Metodo setter de la ruta de la imagen del Platillo
     * @param path tipo String, Nuevo path para la imagen del platillo.
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *Metodo getter los puntos del platillo
     * @return puntos de tipo Int, Hace referencia a los puntos que tendra dicho platillo
     */
    public int getPuntos() {
        return puntos;
    }

    /**
     * Metodo setter de los puntos del platillo
     * @param puntos tipo Int, Nuevo puntos para el platillo.
     */
    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    /**
     * Metodo ToString que permite imprimir todos loa tributos de cada Platillo
     * @return Una cadena de estados correspondientes a los atributos del platillos 
     */
    @Override
    public String toString() {
        return "Platillo{" + "nombre=" + nombre + ", listaProductos=" + listaProductos + ", path=" + path + ", puntos=" + puntos + ", imagen=" + imagen + ", imagenPlatillo=" + imagenPlatillo + '}';
    }

    /**
     *Metodo getter de imagenPlatillo
     * @return imagenPlatillo, Hace referencia a la imagen del platillo
     */
    public ImageView getImagenPlatillo() {
        return imagenPlatillo;
    }

    /**
     *Metodo setter de imagenPlatillo
     * @param imagenPlatillo Hace referencia a la imagen del platillo
     */
    public void setImagenPlatillo(ImageView imagenPlatillo) {
        this.imagenPlatillo = imagenPlatillo;
    }


    
    
}
