/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush_2p;

import constantes.CONSTANTES;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Clase principal del juego, Es el origen de todo el sistema.
 * @author CHRISTIN
 */
public class KitchenRush_2P extends Application {
    
    public static Stage primaryStage; 
    public static Scene scene; 
    
    
    /**
     * Metodo que ejecuta la aplicacion
     * @param primaryStage Stage del proyecto
     */
    @Override
    public void start(Stage primaryStage) {
        
        this.primaryStage = primaryStage; 
        
        PanelInicial panelI = new PanelInicial();
    
        scene = new Scene(panelI.getRoot(),CONSTANTES.ANCHO_JUEGO, CONSTANTES.ALTURA_JUEGO);
        
        primaryStage.setTitle(CONSTANTES.TITULO);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(e-> {
            System.exit(0);
        });
        primaryStage.show();
    }

    /**
     * Es el primer metodo que sera ejecutado por el programa.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
