/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kitchenrush_2p;

import Modelo.Producto;
import Views.*;
import static Views.VistaJuego.listaProductos;
import constantes.CONSTANTES;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.BorderPane;


/**
 * Clase que tiene el BorderPane que contiene todos los elementos que se mostraran en la ventana
 * @author CHRISTIN
 */
public final class PanelInicial {
    private BorderPane root;
    private PanelInicial pInicial;
    private VistaMenu vistaMenu;
    private VistaJuego vistaJuego;
    private VistaIngresoPlatillo vistaIngreso;
    private VistaIngresoProductos vistaIngresoPr;
    
    public static ArrayList<Producto> productosActivos = new ArrayList<Producto> ();
    public static ArrayList<String> productoEnMesa1 = new ArrayList<String> ();
    public static ArrayList<String> productoEnMesa2 = new ArrayList<String> ();

    public static Map<String, String> nombreMap = new HashMap<String, String>();//platillo,ruta
    public static ArrayList<String> nombreProducto = new ArrayList<String> ();//nombre productos
    public static ArrayList<String> nombrePlatillos = new ArrayList<String> ();//nombre platillos
    
    /**
     * Constructor de la clase PanelInicial 
     */
    public PanelInicial(){
        productosActivos.clear();
        pInicial=this;
        root = new BorderPane(); //Para juegos es mejor border pane ya que se distribuye mejor los espacios.                
        cargarRutaProductos(CONSTANTES.PRODUCTOS_PATH);
        cargarRutaPlatillos(CONSTANTES.PLATILLOS_PATH);
        MenuPrincipal();
    }
    
    /**
     * Metodo que permitirá crear la vista del menu principal y darle a los botones de la misma la accion a 
     * realizar
     */
    public void MenuPrincipal(){
        vistaMenu=new VistaMenu(root);
        vistaMenu.getIniciarJ().setOnAction(new Acciones(1));   
        vistaMenu.getSalir().setOnAction(new Acciones(3));
        vistaMenu.getIngresarR().setOnAction(new Acciones(2));
        vistaMenu.getIngresarP().setOnAction(new Acciones(4));
    }
    
    /**
     * Metodo agregara a un mapa (nombreMap) y a una lista (nombreProducto) los productos en el archivo
     * @param fuente ruta del archivo productos.txt
     */
    public void cargarRutaProductos(String fuente){
        ArrayList<String> linea = CargarArchivo(fuente);
        listaProductos = new ArrayList<>();
        for(int i=0;i<linea.size();i++){
            String[] control= linea.get(i).split(",");
            nombreMap.put(control[0], control[1]);
            nombreProducto.add(control[0]);
        }
    }
    
    /**
     * Metodo agregara a una lista (nombrePlatillos) los platillos en el archivo
     * @param fuente ruta del archivo platillo.txt
     */
    public void cargarRutaPlatillos(String fuente){
        ArrayList<String> linea = CargarArchivo(fuente);        
        for(int i=0;i<linea.size();i++){
            String[] control= linea.get(i).split(",");
            nombrePlatillos.add(control[0]);
        }
    }
    
    /**
     * Metodo que permitia cargar un archivo en una lista
     * @param fuente ruta del archivo a leer
     * @return lista que contiene los elementos del archivos
     */
    public ArrayList<String> CargarArchivo(String fuente){
        ArrayList<String> linea = new ArrayList<>();
        BufferedReader inputStream = null;
        String line = null;
        try {
            inputStream = new BufferedReader(new FileReader(fuente));
            while ((line = inputStream.readLine()) != null) {
                linea.add(line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
        } catch (IOException e) {
            System.out.println("Error al momento de leer el archivo.");
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {

            }
        }
        return linea;
    }
    
    /**
     * Metodo que permitira crear una vista del juego
     */
    public void IniciarJuego(){
        vistaJuego = new VistaJuego(root);        
    }
    
    /**
     * Metodo que permitira crear una vista que permitira el ingreso de los platillos al juego
     */
    public void IniciarIngresoPlatillos(){ 
        vistaIngreso = new VistaIngresoPlatillo(root); 
        vistaIngreso.getMenu().setOnAction(new Acciones(5));
    }
    
    /**
     * Metodo que permitira crear una vista que permitira el ingreso de los prouctos al juego
     */
    public void IniciarIngresoProductos(){ 
        vistaIngresoPr = new VistaIngresoProductos(root);   
        vistaIngresoPr.getMenu().setOnAction(new Acciones(5));
    }
       
    
    /**
     * Clase privada que le especificara que debera hacer cada boton dependiendo de la opcion que se pase
     * en el constructor
     */
    private class Acciones implements EventHandler<ActionEvent> {
        
        int opcion;
              
        /**
         * Constructor de la clase privada
         * @param opcion dependiendo de la opcion se realizara una accion diferente
         */
        public Acciones(int opcion) {
            this.opcion = opcion;            
        }        
        
        /**
         * Metodo que le dara una accion a los botones dependiendo de la opcion 
         * @param event evento a realizar por los botones
         */
        public void handle(ActionEvent event) {

            if (this.opcion == 1) {                
                pInicial.IniciarJuego();  
            }
            
            if (this.opcion == 2) {
                pInicial.IniciarIngresoPlatillos();  
            }                     
            
            if (this.opcion == 3) {    
                System.exit(0);                
            }
            
            if (this.opcion == 4) {
                pInicial.IniciarIngresoProductos();  
            }
            
            if (this.opcion == 5) {
                pInicial.MenuPrincipal();  
            }
        }

    }
    
    /**
     * Metodo que permitira obtener el border pane principal
     * @return border pane
     */
    public BorderPane getRoot(){
        return root;
    }
    
}
