/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import Modelo.Jugador;
import Modelo.Platillo;
import Modelo.Producto;
import Views.VistaJuego;
import constantes.CONSTANTES;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import kitchenrush_2p.PanelInicial;

/**
 * Clase que crea el hilo encargado de la creacion de los platillos (Cojer los
 * productos y dejarlos en la mesa de trabajo), cargar.
 *
 * @author CHRISTIN
 */
public class HiloProductoMesaTrabajo extends Thread {

    private Platillo platilloJ1;
    private Platillo platilloJ2;
    private boolean bandera;
    private HBox paneBorder;
    private Pane rootEstacion;
    private Rectangle EstacionTrabajo1;
    private Rectangle EstacionTrabajo2;
    private VBox contenedorProductos1;
    private VBox contenedorProductos2;
    private int caso;
    private Jugador j;
    private int reloj;
    private Timeline timelineTiempo;
    private Timeline TimelinePuntaje;
    private boolean banderaDejarJ1;
    private boolean banderaDejarJ2;

    /**
     * Constructor de la clase hiloProductoMesaTrabajo
     *
     * @param paneBorder BarraSuperior de VistaJuego que se encarga de contener
     * todos la parte superior del juego.
     * @param rootEstacion Pane de VistaJuego en donde el jugador tendra
     * libertad de movimiento y tendra sus areas de trabajos.
     * @param j Hace referencia al jugador
     * @param caso Hace referencia al tipo de jugador.
     */
    public HiloProductoMesaTrabajo(HBox paneBorder, Pane rootEstacion, Jugador j, int caso) {
        this.paneBorder = paneBorder;
        bandera = true;
        this.rootEstacion = rootEstacion;
        this.j = j;
        this.caso = caso;
        contenedorProductos1 = new VBox();
        contenedorProductos2 = new VBox();
        reloj = 0;
        banderaDejarJ1 = true;
        banderaDejarJ2 = true;

    }

    /**
     * Hilo que agrega los productos a la mesa de trabajo de cada jugador
     * dependiendo del platillo. A su vez acepta que el jugador pueda coger los
     * productos que salen aleatoriamente.
     */
    public void run() {
        while (bandera) {
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (caso == 1) {
                            setupTimeTiempoPlatillo(1);

                            contenedorProductos1.getChildren().clear();
                            contenedorProductos1.setLayoutX(VistaJuego.EstacionTrabajo1.getLayoutX() + 25);
                            contenedorProductos1.setLayoutY(VistaJuego.EstacionTrabajo1.getLayoutY());
                            contenedorProductos1.setSpacing(30);

                            platilloJ1 = ObtenerPlatillo();
                            paneBorder.getChildren().set(0, platilloJ1.getImagenPlatillo());
                            PanelInicial.productoEnMesa1.clear();
                            for (int i = 0; i < platilloJ1.getListaProductos().size(); i++) {
                                Producto p1 = new Producto(platilloJ1.getListaProductos().get(i).getNombre(), platilloJ1.getListaProductos().get(i).getPath(), contenedorProductos1);
                                PanelInicial.productoEnMesa1.add(p1.getNombre());
                            }
                            setupTimePuntaje(contenedorProductos1, 1);

                            if (!rootEstacion.getChildren().contains(contenedorProductos1)) {
                                rootEstacion.getChildren().add(contenedorProductos1);
                            }
                        }
                        if (caso == 2) {
                            setupTimeTiempoPlatillo(2);

                            contenedorProductos2.getChildren().clear();
                            contenedorProductos2.setLayoutX(VistaJuego.EstacionTrabajo2.getLayoutX());
                            contenedorProductos2.setLayoutY(VistaJuego.EstacionTrabajo2.getLayoutY());
                            contenedorProductos2.setSpacing(30);

                            platilloJ2 = ObtenerPlatillo();

                            paneBorder.getChildren().set(6, platilloJ2.getImagenPlatillo());
                            PanelInicial.productoEnMesa2.clear();

                            for (int i = 0; i < platilloJ2.getListaProductos().size(); i++) {
                                Producto p2 = new Producto(platilloJ2.getListaProductos().get(i).getNombre(), platilloJ2.getListaProductos().get(i).getPath(), contenedorProductos2);
                                PanelInicial.productoEnMesa2.add(p2.getNombre());

                            }

                            setupTimePuntaje(contenedorProductos2, 2);

                            if (!rootEstacion.getChildren().contains(contenedorProductos2)) {
                                rootEstacion.getChildren().add(contenedorProductos2);
                            }
                        }
                    }
                });
                sleep(30000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloProductoMesaTrabajo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Metodo que crea un platillo aleatorio desde un arreglo de platillos el
     * cual es creado al iniciar el juego desde el .txt
     *
     * @return objeto Platillo.
     */
    public Platillo ObtenerPlatillo() {
        int number = (int) (Math.random() * VistaJuego.listaPlatillos.size());
        Platillo platilloCreado = VistaJuego.listaPlatillos.get(number);
        Platillo platillo = new Platillo(platilloCreado.getNombre(), platilloCreado.getListaProductos(), platilloCreado.getPath(), platilloCreado.getPuntos());
        return platillo;
    }

    /**
     * InnerClass la cual permite crear un hilo para administrar el tiempo de
     * cada platillo
     */
    private class TimeHandlerTiempoPlatillo implements EventHandler<ActionEvent> {

        int t = 29;
        int caso;
        Label l;

        /**
         * Constructor de la innerClass.
         *
         * @param caso hace referencia al jugador que llama al
         * timeHandler(Hilo).
         */
        public TimeHandlerTiempoPlatillo(int caso) {
            this.caso = caso;
        }

        /**
         * Metodo se ejecutara mientras este ejecutandose el timeHandler. Setea
         * un tiempo y va descontanto hasta llegar a 0.
         *
         * @param event
         */
        public void handle(ActionEvent event) {
            if (caso == 1) {
                VBox vb = (VBox) VistaJuego.barraSuperior.getChildren().get(2);
                l = (Label) vb.getChildren().get(1);
                l.setText(String.valueOf(t));
            } else if (caso == 2) {
                VBox vb = (VBox) VistaJuego.barraSuperior.getChildren().get(4);
                l = (Label) vb.getChildren().get(1);
                l.setText(String.valueOf(t));
            }
            if (t == 0) {
            } else {
                t = t - 1;
            }
        }
    }

    /**
     * TimeHandler que actua como un hilo y ejecuta TimeHandlerTiempoPlatillo
     * casa 1 segundo.
     *
     * @param caso hace referencia al jugador.
     */
    public void setupTimeTiempoPlatillo(int caso) {
        KeyFrame kf = new KeyFrame(javafx.util.Duration.seconds(1), new TimeHandlerTiempoPlatillo(caso));
        timelineTiempo = new Timeline(kf);
        timelineTiempo.setCycleCount(29);
        timelineTiempo.play();
    }

    /**
     * InnerClass la cual permite crear un hilo para administrar el puntaje de
     * cada jugador.
     */
    private class TimeHandlerPuntaje implements EventHandler<ActionEvent> {

        int t = 29;
        int caso;
        Label l;
        VBox vb;

        /**
         * Metodo contructor de TImeHandlerPuntaje
         * @param vb contenedor el cual tiene el panel superior del juego.
         * @param caso Hace referencia al jugador.
         */
        public TimeHandlerPuntaje(VBox vb, int caso) {
            this.vb = vb;
            this.caso = caso;
        }

        /**
         * Metodo se ejecutara mientras este ejecutandose el timeHandler.
         * Permite coger objetos desde la parte inferior y llevarlos a la mesa
         * de trabajo de cada jugador respectivamente. Se administran acciones
         * al coger/dejar producto y completar el platillo.
         *
         * @param event
         */
        public void handle(ActionEvent event) {
            synchronized (this) {
                //cojer
                for (int i = 0; i < PanelInicial.productosActivos.size(); i++) {
                    if (j.getImagenJugador().getBoundsInParent().intersects(PanelInicial.productosActivos.get(i).getImagen().getBoundsInParent())) {
                        if (j.getProducto() == null || (j.getProducto().getNombre() == null)) {
                            if (caso == 1) {
                                banderaDejarJ1 = true;
                            } else if (caso == 2) {
                                banderaDejarJ2 = true;
                            }
                            j.setProducto(PanelInicial.productosActivos.get(i));
                            rootEstacion.getChildren().remove(PanelInicial.productosActivos.get(i).getImagen());
                            PanelInicial.productosActivos.remove(i);
                        }
                    }
                }
                //depositar
                try {
                    if (caso == 1) {
                        if ((j.getImagenJugador().getLayoutX() - CONSTANTES.VELOCIDAD_JUGADOR) < (VistaJuego.EstacionTrabajo1.getLayoutX() + VistaJuego.EstacionTrabajo1.getWidth())) {
                            for (int i = 0; i < vb.getChildren().size(); i++) {
                                if (PanelInicial.productoEnMesa1.get(i).equalsIgnoreCase(j.getProducto().getNombre())) {
                                    contenedorProductos1.getChildren().remove(i);
                                    PanelInicial.productoEnMesa1.remove(i);
                                }
                            }
                            System.out.println("lo deja");
                            j.setProducto(new Producto());
                            if (PanelInicial.productoEnMesa1.isEmpty() && banderaDejarJ1) {
                                VBox vb = (VBox) VistaJuego.barraSuperior.getChildren().get(1);
                                l = (Label) vb.getChildren().get(1);
                                HBox Imagenes = (HBox) vb.getChildren().get(2);
                                Platillo pla = new Platillo(platilloJ1.getNombre(), platilloJ1.getListaProductos(), platilloJ1.getPath(), platilloJ1.getPuntos());
                                pla.getImagenPlatillo().setFitHeight(20);
                                pla.getImagenPlatillo().setFitWidth(20);
                                Imagenes.getChildren().add(pla.getImagenPlatillo());
                                j.setPuntaje(j.getPuntaje() + platilloJ1.getPuntos());
                                l.setText(String.valueOf(j.getPuntaje()));
                                banderaDejarJ1 = false;

                            }
                        }
                    } else if (caso == 2) {
                        if ((j.getImagenJugador().getLayoutX() + CONSTANTES.ANCHO_JUGADOR + CONSTANTES.VELOCIDAD_JUGADOR) > (VistaJuego.EstacionTrabajo2.getLayoutX())) {

                            for (int i = 0; i < vb.getChildren().size(); i++) {
                                if (PanelInicial.productoEnMesa2.get(i).equalsIgnoreCase(j.getProducto().getNombre())) {
                                    contenedorProductos2.getChildren().remove(i);
                                    PanelInicial.productoEnMesa2.remove(i);
                                }
                            }
                            
                            
                            if (PanelInicial.productoEnMesa2.isEmpty() && banderaDejarJ2) {
                                VBox vb = (VBox) VistaJuego.barraSuperior.getChildren().get(5);
                                l = (Label) vb.getChildren().get(1);

                                HBox ImagenesJ2 = (HBox) vb.getChildren().get(2);
                                Platillo plaJ2 = new Platillo(platilloJ2.getNombre(), platilloJ2.getListaProductos(), platilloJ2.getPath(), platilloJ2.getPuntos());
                                plaJ2.getImagenPlatillo().setFitHeight(20);
                                plaJ2.getImagenPlatillo().setFitWidth(20);
                                ImagenesJ2.getChildren().add(plaJ2.getImagenPlatillo());

                                j.setPuntaje(j.getPuntaje() + platilloJ2.getPuntos());
                                l.setText(String.valueOf(j.getPuntaje()));
                                banderaDejarJ2 = false;

                            }

                            j.setProducto(new Producto());
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * TimeHandler que actua como un hilo y ejecuta TimeHandlerPuntaje
     * @param vb contenedor que contiene los productos necesarios para completar un platillo.
     * @param caso Hace referencia al jugador
     */
    public void setupTimePuntaje(VBox vb, int caso) {
        KeyFrame kf = new KeyFrame(javafx.util.Duration.seconds(1), new TimeHandlerPuntaje(vb, caso));
        TimelinePuntaje = new Timeline(kf);
        TimelinePuntaje.setCycleCount(29);
        TimelinePuntaje.play();
    }

    /**
     *Metodo getter de TimelineTiempo
     * @return objeto TimelineTiempo
     */
    public Timeline getTimelineTiempo() {
        return timelineTiempo;
    }

    /**
     * Metodo setter de TimelineTiempo
     * @param timelineTiempo timeline que regula el tiempo de cada platillo
     */
    public void setTimelineTiempo(Timeline timelineTiempo) {
        this.timelineTiempo = timelineTiempo;
    }

    /**
     *Metodo getter de Bandera.
     *Bandera define si las acciones del hilo se realizan o no.
     * @return boolean.
     */
    public boolean isBandera() {
        return bandera;
    }

    /**
     * Metodo setter de Bandera.
     * @param bandera valor que setea el valor de bandera.
     */ 
    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    public boolean isBanderaDejarJ1() {
        return banderaDejarJ1;
    }

    public void setBanderaDejarJ1(boolean banderaDejarJ1) {
        this.banderaDejarJ1 = banderaDejarJ1;
    }

    public boolean isBanderaDejarJ2() {
        return banderaDejarJ2;
    }

    public void setBanderaDejarJ2(boolean banderaDejarJ2) {
        this.banderaDejarJ2 = banderaDejarJ2;
    }

    public VBox getContenedorProductos1() {
        return contenedorProductos1;
    }

    public void setContenedorProductos1(VBox contenedorProductos1) {
        this.contenedorProductos1 = contenedorProductos1;
    }

    public VBox getContenedorProductos2() {
        return contenedorProductos2;
    }

    public void setContenedorProductos2(VBox contenedorProductos2) {
        this.contenedorProductos2 = contenedorProductos2;
    }

    public Timeline getTimelinePuntaje() {
        return TimelinePuntaje;
    }

    public void setTimelinePuntaje(Timeline TimelinePuntaje) {
        this.TimelinePuntaje = TimelinePuntaje;
    }
    
    public void limpiarHilo(){
        
    }
    
}
