/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;
import Modelo.*;
import Views.VistaJuego;
import constantes.CONSTANTES;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que crea el hilo para el movimiento el jugador.
 * @author CHRISTIN
 */
public class HiloMovimiento extends Thread{
    private Jugador j1;
    private Jugador j2;
    private boolean bandera = true;

    /**
     *Constructor por defecto
     */
    public HiloMovimiento() {
    }

    /**
     *Constructor de la clase HiloMovimiento
     * @param j1 Hace referencia al jugador#1
     * @param j2 Hace referencia al Jugador#2
     */
    public HiloMovimiento(Jugador j1, Jugador j2) {
        this.j1 = j1;
        this.j2 = j2;
    }
    
    /**
     *Metodo que verifica que el siguiente movimiento de mi jugador este dentro del area en la cual tiene permitido caminar.
     * @param caso Hace referencia a la direccion que tiene el jugador. 1 arriba, 2 abajo, 3 izquierda, 4 derecha.
     * @param jugador Hace referencia al jugador.
     * @return booleno que verifica si el movimiento es valido, caso contrario el jugador no se podra mover en cierta direccion.
     */
    public boolean confirmarMovimientoTope(int caso, Jugador jugador) {
        switch (caso) {
            case 1:
                if (jugador.getImagenJugador().getLayoutY() - CONSTANTES.VELOCIDAD_JUGADOR >= 0) {
                    return true;
                }
                break;
            case 2:
                if (jugador.getImagenJugador().getLayoutY() + CONSTANTES.ALTURA_JUGADOR < (jugador.getRoot().getHeight()-60)) {
                    return true;
                }
                break;
            case 3:
                if (jugador.getImagenJugador().getLayoutX() > 0) {
                    return true;
                }
                break;
            case 4:
                if (jugador.getImagenJugador().getLayoutX() + jugador.getImagenJugador().getFitWidth() < CONSTANTES.ANCHO_JUEGO) {
                    return true;
                }
                break;
        }
        return false;
    }
    
    /**
     * Metodo que verifica que los jugadores no choquen con la mesa de trabajo.
     * @param j Hace referencia al jugador.
     * @return boolean que verifica si el movimiento es valido.
     */
    public boolean choqueMesaTrabajo(Jugador j){
        if(j.getImagenJugador().getBoundsInParent().intersects(VistaJuego.EstacionTrabajo1.getBoundsInParent())){
           j.getImagenJugador().setLayoutX(j.getImagenJugador().getLayoutX()+CONSTANTES.VELOCIDAD_JUGADOR);
            return false;
        }
        if(j.getImagenJugador().getBoundsInParent().intersects(VistaJuego.EstacionTrabajo2.getBoundsInParent())){
          j.getImagenJugador().setLayoutX(j.getImagenJugador().getLayoutX()-CONSTANTES.VELOCIDAD_JUGADOR);
           return false;
        }
       
        return true;
    }
    
    /**
     * Metodo que se ejecutara siempre que el hilo este activo.
     * Se encarga de verificar las teclas presionadas por los jugadores y respecto a eso mover al jugador.
     */
    public void run() {
        while (bandera) {
            try { 
                if (!Jugador.teclasPresionadas.isEmpty()&& choqueMesaTrabajo(j1) && choqueMesaTrabajo(j2)) {
                    //si el jugador va hacia arriba j1
                    if (Jugador.teclasPresionadas.contains(j1.getControles()[0]) && confirmarMovimientoTope(1, j1)) { 
                        double posicion = j1.getImagenJugador().getLayoutY() - CONSTANTES.VELOCIDAD_JUGADOR;
                        j1.getImagenJugador().setLayoutY(posicion); 
                    }
                    //si el jugador va hacia arriba j2
                    if (Jugador.teclasPresionadas.contains(j2.getControles()[0]) && confirmarMovimientoTope(1, j2)) {
                        double posicion = j2.getImagenJugador().getLayoutY() - CONSTANTES.VELOCIDAD_JUGADOR;
                         j2.getImagenJugador().setLayoutY(posicion);
                    }
                    //si el jugador va hacia abajo j1
                    if (Jugador.teclasPresionadas.contains(j1.getControles()[1]) && confirmarMovimientoTope(2, j1)) { 
                        double posicion = j1.getImagenJugador().getLayoutY() + CONSTANTES.VELOCIDAD_JUGADOR;
                        j1.getImagenJugador().setLayoutY(posicion);
                    }
                   //si el jugador va hacia abajo j2  
                    if (Jugador.teclasPresionadas.contains(j2.getControles()[1]) && confirmarMovimientoTope(2, j2) ) {     
                        double posicion = j2.getImagenJugador().getLayoutY() + CONSTANTES.VELOCIDAD_JUGADOR;
                        j2.getImagenJugador().setLayoutY(posicion);        
                    }
                     //si el jugador va hacia izquierda j1
                    if (Jugador.teclasPresionadas.contains(j1.getControles()[3]) && confirmarMovimientoTope(3, j1)) {
                        double posicion = j1.getImagenJugador().getLayoutX() - CONSTANTES.VELOCIDAD_JUGADOR;
                        j1.getImagenJugador().setLayoutX(posicion);                   
                    }
                    //si el jugador va hacia izquierda j2
                    if (Jugador.teclasPresionadas.contains(j2.getControles()[3]) && confirmarMovimientoTope(3, j2) ) { 
                        double posicion = j2.getImagenJugador().getLayoutX() - CONSTANTES.VELOCIDAD_JUGADOR;
                        j2.getImagenJugador().setLayoutX(posicion);                       
                    }
                    //si el jugador va hacia derecha j1
                    if (Jugador.teclasPresionadas.contains(j1.getControles()[2]) && confirmarMovimientoTope(4, j1) ) { 
                        double posicion = j1.getImagenJugador().getLayoutX() + CONSTANTES.VELOCIDAD_JUGADOR;
                         j1.getImagenJugador().setLayoutX(posicion);   
                    }
                    //si el jugador va hacia derecha j2
                    if (Jugador.teclasPresionadas.contains(j2.getControles()[2]) && confirmarMovimientoTope(4, j2) ) {
                        double posicion = (j2.getImagenJugador().getLayoutX() + CONSTANTES.VELOCIDAD_JUGADOR);
                        j2.getImagenJugador().setLayoutX(posicion);  
                    }
                }
                sleep(100);

            } catch (InterruptedException ex) {
                Logger.getLogger(HiloMovimiento.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }
    }

    /**
     *Metodo getter de Bandera.
     *Bandera define si las acciones del hilo se realizan o no.
     * @return boolean.
     */
    public boolean isBandera() {
        return bandera;
    }

    /**
     * Metodo setter de Bandera.
     * @param bandera valor que setea el valor de bandera.
     */ 
    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }
 
    
}
