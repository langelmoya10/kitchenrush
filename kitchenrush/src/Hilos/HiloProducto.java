/* 
* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import Modelo.Producto;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.layout.Pane;
import kitchenrush_2p.PanelInicial;

/**
 * Clase que crea el hilo para la creacion de productos en la parte inferior del
 * tablero.
 *
 * @author CHRISTIN
 */
public class HiloProducto extends Thread {

    private Producto producto;
    private boolean bandera;
    private Pane pane;

    public HiloProducto(Pane pane) {
        this.pane = pane;
        bandera = true;
    }

    /**
     * Constructor por defecto.
     */
    public HiloProducto() {

    }
    /**
     * Metodo que se ejecutara siempre que el hilo este activo. Se encargar de
     * generar productos aleatoriamente y ubicarlos en una posicion aleatoria en
     * X.
     */
    public void run() {
        while (bandera) {
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        int indice = (int) (PanelInicial.nombreProducto.size() * Math.random());
                        producto = new Producto(PanelInicial.nombreProducto.get(indice), PanelInicial.nombreMap.get(PanelInicial.nombreProducto.get(indice)), pane);
                        PanelInicial.productosActivos.add(producto);    
                        if (PanelInicial.productosActivos.size() == 20) {
                            for (int i = 1; i < (PanelInicial.productosActivos.size()/2); i++) {
                                pane.getChildren().remove(PanelInicial.productosActivos.get(i).getImagen());
                                PanelInicial.productosActivos.remove(i);

                            }
                        }
                    }
                });
                sleep(2500);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloProducto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Metodo getter de Bandera. Bandera define si las acciones del hilo se
     * realizan o no.
     *
     * @return boolean.
     */
    public boolean isBandera() {
        return bandera;
    }

    /**
     * Metodo setter de Bandera.
     *
     * @param bandera valor que setea el valor de bandera.
     */
    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

}
