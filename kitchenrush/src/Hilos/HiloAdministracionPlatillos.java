/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import static Views.VistaJuego.barraSuperior;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.layout.Pane;
import Modelo.Jugador;
import Modelo.Platillo;
import Views.VistaJuego;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import kitchenrush_2p.PanelInicial;

/**
 *
 */
public class HiloAdministracionPlatillos extends Thread {

    private HiloProductoMesaTrabajo hiloProductoMesaTrabajo;
    private boolean bandera;
    private HiloProductoMesaTrabajo hpmt1;
    private HiloProductoMesaTrabajo hpmt2;
    private int caso;
    private HBox paneBorder;
    private Pane rootEstacion;
    private Jugador j;

    public HiloAdministracionPlatillos(HBox paneBorder, Pane rootEstacion, Jugador j, int caso) {
        this.paneBorder = paneBorder;
        this.rootEstacion = rootEstacion;
        this.j = j;
        this.caso = caso;
        bandera = true;
        if(caso==1){
            hpmt1 = new HiloProductoMesaTrabajo(barraSuperior, rootEstacion, j, 1);
        }else if(caso==2){
           hpmt2 = new HiloProductoMesaTrabajo(barraSuperior, rootEstacion, j, 2);
        }
    }

    public void run() {
        System.out.println("corriend");
        while (bandera) {
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            if (caso == 1 && PanelInicial.productoEnMesa1.isEmpty()) {
                                System.out.println("entro 1 if");
                                // System.out.println("El hilo esta vivo> "+ hpmt1.isAlive());
                                    if (hpmt1.isAlive() && PanelInicial.productoEnMesa1.isEmpty()) {
                                        //System.out.println("entreo 3 if");
                                        hpmt1.getContenedorProductos1().getChildren().clear();
                                        hpmt1.getTimelinePuntaje().stop();
                                        hpmt1.getTimelineTiempo().stop();
                                        hpmt1.stop();
                                        /*System.out.println("");
                                    System.out.println(hpmt1.getTimelineTiempo().getStatus());
                                    System.out.println("");   
                                         */
                                     //   System.out.println("se crea un nuevo hilo ");
                                        hpmt1 = new HiloProductoMesaTrabajo(barraSuperior, rootEstacion, j, 1);
                                        hpmt1.start();
                                    }

                                if (!hpmt1.isAlive()) {
                                    hpmt1.start();
                                }

                            } else if (caso == 2 && PanelInicial.productoEnMesa2.isEmpty()) {
                                // System.out.println("entro 1 if");
                                // System.out.println("El hilo esta vivo> "+ hpmt1.isAlive());
                                    if (hpmt2.isAlive() && PanelInicial.productoEnMesa2.isEmpty()) {
                                        //System.out.println("entreo 3 if");
                                        hpmt2.getContenedorProductos1().getChildren().clear();
                                        hpmt2.getTimelinePuntaje().stop();
                                        hpmt2.getTimelineTiempo().stop();
                                        hpmt2.stop();
                                        /*System.out.println("");
                                    System.out.println(hpmt1.getTimelineTiempo().getStatus());
                                    System.out.println("");   
                                         */
                                        System.out.println("se crea un nuevo hilo ");
                                        hpmt2 = new HiloProductoMesaTrabajo(barraSuperior, rootEstacion, j, 2);
                                        hpmt2.start();
                                    }

                                if (!hpmt2.isAlive()) {
                                    System.out.println("entreo 2 if");
                                    hpmt2.start();
                                }

                            }
                        }catch(Exception e){
                            Logger.getLogger(HiloAdministracionPlatillos.class.getName()).log(Level.SEVERE, null, e);
                        }
                    }
                });
                sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloAdministracionPlatillos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public HiloProductoMesaTrabajo getHiloProductoMesaTrabajo() {
        return hiloProductoMesaTrabajo;
    }

    public void setHiloProductoMesaTrabajo(HiloProductoMesaTrabajo hiloProductoMesaTrabajo) {
        this.hiloProductoMesaTrabajo = hiloProductoMesaTrabajo;
    }

    public boolean isBandera() {
        return bandera;
    }

    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    public HiloProductoMesaTrabajo getHpmt1() {
        return hpmt1;
    }

    public void setHpmt1(HiloProductoMesaTrabajo hpmt1) {
        this.hpmt1 = hpmt1;
    }

    public HiloProductoMesaTrabajo getHpmt2() {
        return hpmt2;
    }

    public void setHpmt2(HiloProductoMesaTrabajo hpmt2) {
        this.hpmt2 = hpmt2;
    }

    public int getCaso() {
        return caso;
    }

    public void setCaso(int caso) {
        this.caso = caso;
    }

    public HBox getPaneBorder() {
        return paneBorder;
    }

    public void setPaneBorder(HBox paneBorder) {
        this.paneBorder = paneBorder;
    }

    public Pane getRootEstacion() {
        return rootEstacion;
    }

    public void setRootEstacion(Pane rootEstacion) {
        this.rootEstacion = rootEstacion;
    }

    public Jugador getJ() {
        return j;
    }

    public void setJ(Jugador j) {
        this.j = j;
    }
    
    

}
